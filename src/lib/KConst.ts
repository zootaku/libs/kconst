import * as path from "path";
import {FileUtil} from "zoo.util/lib/file-util";
import {ProxyUtil} from "zoo.util/lib/proxy-util";
import { Target } from "./target";
import * as os from "os";
import {KConstCollector, CollectorProps, OnSet, Override, SELF_NAME} from "./KConstCollector";

export enum SelectorItem {
    MODE="mode",
    USER="user",
    MODE_USER="mode_user",
    DEFAULT="*"
}

const  DEFAULT_PRIORITY:SelectorItem[] = [ SelectorItem.DEFAULT, SelectorItem.MODE, SelectorItem.USER, SelectorItem.MODE_USER ];

type DeclareCallback = ( exports:( ...targets )=> void, override:Override, SELF_NAME, props:CollectorProps )=>void;
type EventCallback = OnSet;
type TargetCallback = ( target:Target )=>void;
type TriggerCallback = ()=>void;

export enum FormatterType {
    REAL_NAME="REAL_NAME",
    UPPER="UPPER",
    LOWER="LOWER",
}

export type KConstArgs = {
    user:string,
    root:string,
    mode:BuildMode,
    name?: string,
    exports?:string,
    targets?:string,
    triggers?:string,
    events?:string
    formatter?: FormatterType,
    priorityOrder?:SelectorItem[]
}

export type Selector = {
    user?:string|string[],
    mode?:BuildMode|BuildMode[],
    label?:string
}

export type BuildMode = "dev" | "prod" | "local" | "remote" | "build" | string;


export enum TriggersMethod {
    DECLARE = "initialize",
    PUBLISH = "publish"
}

export enum TriggerEvent {
    BEFORE="before",
    AFTER="after"
}

enum Callback{
    DECLARES="declares",
    TRIGGERS="triggers",
    EVENTS="events",
    TARGETS="targets"
}


export enum Declaration {
    CREATE,
    OVERRIDE,
}
export type OnSelector = Selector & {
    applyOn?:string|string[],
    declarations?:Declaration|Declaration[]
}

export class KConst {

    private configs:KConstArgs;
    private _on:any[] = ProxyUtil.infinityProxyArray();

    private readonly _target: Target;
    private readonly _exports: ()=>void;
    private readonly _exportator:KConstCollector;

    constructor( configs:KConstArgs ){
        this.configs = {
            formatter: configs.formatter || FormatterType.REAL_NAME,
            mode: configs.mode,
            user: configs.user || os.userInfo().username,
            name: configs.name ||  "K",
            root: configs.root,
            targets: configs.targets || "targets",
            exports: configs.exports || "exports",
            triggers: configs.triggers || "triggers",
            events: configs.triggers || "events",
            priorityOrder: configs.priorityOrder
        };

        if( !this.configs.priorityOrder ) this.configs.priorityOrder = [];
        else if(  !Array.isArray( this.configs.priorityOrder ) ) this.configs.priorityOrder = [ this.configs.priorityOrder ];

        DEFAULT_PRIORITY.forEach( (value, index) => {
            if( !this.configs.priorityOrder.includes( value ) && Array.isArray( this.configs.priorityOrder ) )
                this.configs.priorityOrder.push( value );
        })

        this.configs.priorityOrder = this.configs.priorityOrder.filter( (value, index, array) =>  array.indexOf( value ) === index );
        this._exportator =  new KConstCollector( (key, value, replace) => {
            this.executeEvents( key, value, replace );
            return true;
        });

        this._target = new Target();
    }

    public get exports():(()=>void){ return  this._exports; }

    private _acceptSelector( selector: Selector ):boolean{
        let mode:BuildMode[], user:string[];
        const rejection:string[] = [];

        if( selector && typeof selector.mode === "string" && selector.mode.trim().length === 0 ) selector.mode = null;
        if( selector && typeof selector.user === "string" && selector.user.trim().length === 0 ) selector.user = null;

        if( selector && selector.mode && Array.isArray( selector.mode ) ) mode = selector.mode;
        else if( selector && selector.mode && !Array.isArray( selector.mode  ) ) mode = [ selector.mode ];

        if( selector && selector.user && Array.isArray( selector.user ) ) user = selector.user;
        else if( selector && selector.user && !Array.isArray( selector.user ) ) user = [ selector.user ];


        if( mode && mode.length > 0 && !mode.includes( this.configs.mode ) ) rejection.push( "mode" );
        if( user && user.length > 0 && !user.includes( this.configs.user ) ) rejection.push( "user" );

        return rejection.length === 0;
    }

    target(callback:TargetCallback, selector?:Selector  ){
        const collector = this._selectedGroup( Callback.TARGETS, selector );
        if( collector ) collector.push( callback );
    }

    private _selectedGroup( group:string, selector:Selector ):false|( DeclareCallback|TargetCallback|OnSet|TriggerCallback )[]{
        if( this._acceptSelector( selector ) ){
            if( selector && selector.user && selector.mode ) return this._on[ group ][ SelectorItem.MODE_USER ];
            else if ( selector && selector.mode ) return this._on[ group ][ SelectorItem.MODE ];
            else if( selector &&  selector.user ) return this._on[ group ][ SelectorItem.USER ]
            else return this._on[ group ][ SelectorItem.DEFAULT ]
        } else return false;
    }

    trigger( method: TriggersMethod, event: TriggerEvent, callback:TriggerCallback, selector?:Selector ){
        const collector = this._selectedGroup( Callback.TRIGGERS, selector );
        if( collector ) collector[ method ][ event ].push( callback );
    }

    public declares( callback:DeclareCallback, selector?:Selector ){
        const collector = this._selectedGroup( Callback.DECLARES, selector );
        if( collector ) collector.push( callback );
    }

    public on(callback?:EventCallback, selector?:OnSelector  ){
        const  collector = this._selectedGroup( Callback.EVENTS, selector );
        let applyOn:string[] = [];

        if( selector && selector.applyOn && typeof selector.applyOn === "string" ) applyOn.push(  selector.applyOn );
        else if( selector && Array.isArray( selector.applyOn ) ) applyOn.push( ...selector.applyOn );

        if( collector && applyOn.length > 0 ) applyOn.forEach( onKConst => collector[ "/" ] [ onKConst ].push( callback ) )
        else if( collector && typeof callback === "function" ) collector[ "*" ].push( callback );
    }

    private dirs = {
        root:()=> this.configs.root,
        targets : () => path.join( this.configs.root, this.configs.targets ),
        exports : () => path.join( this.configs.root, this.configs.exports ),
        triggers : () => path.join( this.configs.root, this.configs.triggers ),
        events : () => path.join( this.configs.root, this.configs.events ),
    }

    private createTargets(){
        console.log( `execute targets..`)
        const targets:(( target:Target )=>void)[] = this._priorityList( Callback.TARGETS );
        targets.forEach( value => value( this._target ) );
        console.log( `execute targets... ok!`)
    }

    private executeTriggers(  method:TriggersMethod, event:TriggerEvent ){
        console.log( `execute triggers ${ event } ${ method }...`)
        let callbacks = this._priorityList( Callback.TRIGGERS );
        const triggers:(()=>void)[] = callbacks[ method ][ event ];
        triggers.forEach( value => value() );
        console.log( `execute triggers ${ event } ${ method }... ok!`)
    }

    private _priorityList( group ){
        const priority = ProxyUtil.infinityProxyArray();
        this.configs.priorityOrder.forEach( (value, index, array) => {
            priority.push( ...this._on[ group ][ value ])
        });
        return priority;
    }

    private _priorityGroup( group:Callback ){
        const priority = ProxyUtil.infinityProxyArray();
        this.configs.priorityOrder.forEach( (value, index, array) => {
            priority.push( this._on[ group ][ value ] )
        });
        return priority;
    }

    private executeDeclarations(){
        console.log( `execute declarations...`)
        const declarations:DeclareCallback[] = this._priorityList( Callback.DECLARES )
        declarations.forEach( value => {
            this._exportator.props.restrictBlock();
            value( this._exportator.props.exports, this._exportator.props.override, SELF_NAME, this._exportator.props )
            this._exportator.props.restrictBlock();
        });
        console.log( `execute declarations... ok!`)
    }

    private executeEvents( key:string|number|symbol, value:any, replace:boolean ){
        console.log( `execute declarations...` )
        const events:any = this._priorityGroup( Callback.EVENTS );

        const allEvents = [];
        allEvents.push( ...(events[ "/" ][ key ]) );
        allEvents.push( ...events[ "*" ] );
        for( let i = 0; i< allEvents.length; i++ ){
            if( allEvents[ i ]( key, value, replace ) ) break;
        }
        console.log( `execute declarations... ok!`)
    }

    get K():any{
        return this._exportator.K;
    }

    public collectCallbacks(){
        const collections = [
            { name: "target", group:"targets", filter: /.*.target.js$/, dir: this.dirs.targets() },
            { name: "trigger", group:"triggers", filter: /.*.trigger.js$/, dir: this.dirs.triggers() },
            { name: "export",  group:"exports",  filter: /.*.export.js$/,  dir: this.dirs.exports() },
            { name: "event",   group:"events",   filter: /.*.event.js$/,   dir: this.dirs.triggers() },
        ]

        console.log( `loading [*.kconst.js]...`);
        FileUtil.loadAllFiles( this.dirs.root(), /.*.kconst.js$/, fileRef => {
            require( fileRef.path )
            console.log( `kconst: ${ fileRef.relative } [loaded]`)
        }, { recursive: true })
        console.log( `loading [*.kconst.js]... ok!`);

        collections.forEach( (value, index) => {
            console.log( `loading [ ${ value.group }]...`);
            FileUtil.loadAllFiles( value.dir, value.filter,    fileRef => {
                require( fileRef.path );
                console.log( `trigger: ${ fileRef.relative } [loaded]`)
            }, { recursive: true } )
            console.log( `loading [ ${ value.group }]... ok!` );
        });
    }

    startDeclarations( ){
        console.log( "initializing..." );
        this.createTargets();
        this.executeTriggers( TriggersMethod.DECLARE, TriggerEvent.BEFORE );
        this.executeDeclarations();
        this.executeTriggers( TriggersMethod.DECLARE, TriggerEvent.AFTER );
        console.log( "initializing... ok!" );
    }

    publish() {
        console.log( "publishing..." );
        this.executeTriggers( TriggersMethod.PUBLISH, TriggerEvent.BEFORE );
        this._exportator.publish();

        this.executeTriggers( TriggersMethod.PUBLISH, TriggerEvent.AFTER );
        console.log( "publishing... ok!" );
    }
}