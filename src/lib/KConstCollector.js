"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KConstCollector = exports.SELF_NAME = void 0;
var SelfName = /** @class */ (function () {
    function SelfName() {
    }
    Object.defineProperty(SelfName.prototype, "code", {
        get: function () { return 982145; },
        enumerable: false,
        configurable: true
    });
    return SelfName;
}());
exports.SELF_NAME = new SelfName();
var KConstCollector = /** @class */ (function () {
    function KConstCollector(callback) {
        this.builders = [];
        var self = this;
        this._on = callback;
        function setLastField(p) {
            self._lastField = p;
        }
        var translate = function (object) {
            var translated = {};
            Object.keys(object).forEach(function (key) {
                var value = object[key];
                if (value === exports.SELF_NAME)
                    value = key;
                else if (typeof value === "object" && !Array.isArray(value))
                    value = translate(value);
                translated[key] = value;
            });
            return translated;
        };
        var can = {};
        var canOverride = function (target, p) {
            return can.override || can.overrideBlock;
        };
        this._KConst = new Proxy({}, {
            get: function (target, p) {
                var value = target[p];
                if (value === exports.SELF_NAME)
                    value = p;
                else if (typeof value === "object" && !Array.isArray(value))
                    value = translate(value);
                return value;
            },
            // @ts-ignore
            set: function (target, p, value) {
                var oldValue = target[p];
                // @ts-ignore
                if (canOverride(target, p) || Object.keys(target).indexOf(p) === -1) {
                    target[p] = value;
                    self._on(p, value, oldValue, canOverride(target, p));
                    setLastField(p);
                    return true;
                }
                else {
                    // @ts-ignore
                    throw new Error("const " + p + " already exported");
                }
            }
        });
        var props = {
            K: self.K,
            exportsAll: function (kv) {
                var _this = this;
                var builders = [];
                for (var _i = 1; _i < arguments.length; _i++) {
                    builders[_i - 1] = arguments[_i];
                }
                if (!kv)
                    throw new Error("kv is not object");
                if (typeof kv !== "object")
                    throw new Error("kv is not object");
                Object.keys(kv).forEach(function (value, index) {
                    _this.K[value] = kv[value];
                    props.exports.apply(props, builders);
                });
            },
            exports: function () {
                var builders = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    builders[_i] = arguments[_i];
                }
                self._lastBuilders = builders;
                builders.forEach(function (next) {
                    if (!next)
                        return;
                    if (self.builders.indexOf(next) === -1)
                        self.builders.push(next);
                    next.add({ key: self._lastField, value: self.lastCollected() });
                });
            },
            override: function (replacer) {
                if (typeof replacer !== "function")
                    throw new Error("replacer is not function");
                can.override = true;
                replacer();
                self.builders.forEach(function (next) { return next.removeIf(self._lastField); });
                props.exports.apply(props, self._lastBuilders);
                can.override = false;
            },
            overrideBlock: function () { can.overrideBlock = true; },
            restrictBlock: function () { can.overrideBlock = false; }
        };
        this._exportsProps = props;
    }
    KConstCollector.prototype.lastCollected = function () { return this._KConst[this._lastField]; };
    Object.defineProperty(KConstCollector.prototype, "K", {
        get: function () { return this._KConst; },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(KConstCollector.prototype, "props", {
        get: function () {
            return this._exportsProps;
        },
        enumerable: false,
        configurable: true
    });
    KConstCollector.prototype.publish = function () {
        var builders = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            builders[_i] = arguments[_i];
        }
        if (!builders || builders.length === 0)
            builders = this.builders;
        for (var i = 0; i < builders.length; i++) {
            builders[i].save();
        }
        for (var i = 0; i < builders.length; i++) {
            builders[i].onFinallyPublished();
        }
    };
    return KConstCollector;
}());
exports.KConstCollector = KConstCollector;
//# sourceMappingURL=KConstCollector.js.map