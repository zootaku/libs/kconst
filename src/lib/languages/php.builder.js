"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PhpBuilder = void 0;
var file_system_builder_1 = require("./file-system.builder");
var PhpBuilder = /** @class */ (function (_super_1) {
    __extends(PhpBuilder, _super_1);
    function PhpBuilder(args) {
        var _this = this;
        var _super;
        _super = _this = _super_1.call(this, args) || this;
        _this._super = _super;
        _this.languageName = "PhpBuilder";
        return _this;
    }
    PhpBuilder.prototype.structure = function () {
        var text = "";
        var i = 0;
        console.log(this.fields);
        this.fields.forEach(function (_a) {
            var key = _a.key, value = _a.value;
            var tNext = "    const " + key + " = " + JSON.stringify(value) + ";";
            // if( ++i < this.fields.length ) tNext+=";";
            text += "\n" + tNext;
        });
        return "<?php\n class " + this.props().className + " {\n" + text + "\n\n};";
    };
    ;
    PhpBuilder.prototype.save = function () {
        var fullPath;
        if (this.props().dir)
            fullPath = "" + this.props().dir;
        return this.saveFile({
            dir: this.props().dir,
            fileName: this.props().className + ".php",
        });
    };
    ;
    return PhpBuilder;
}(file_system_builder_1.FileSystemBuilder));
exports.PhpBuilder = PhpBuilder;
//# sourceMappingURL=php.builder.js.map