"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Builder = void 0;
var Builder = /** @class */ (function () {
    function Builder(a) {
        /**
         * @type {{key:string, value:*}[]}
         */
        this.fields = [];
        this.languageName = "LanguageBuilder";
        this._props = a;
    }
    /**
     * @param key
     * @param value
     */
    Builder.prototype.add = function (_a) {
        var key = _a.key, value = _a.value;
        var index = this.find(key).index;
        if (index >= 0)
            this.fields[index] = { key: key, value: value };
        else
            this.fields.push({ key: key, value: value });
    };
    ;
    Builder.prototype.removeIf = function (key) {
        var index = this.find(key).index;
        if (index !== -1)
            this.fields.splice(index, 1);
        return index;
    };
    Builder.prototype.find = function (key) {
        var lastIndex;
        var item = this.fields.find(function (value, index) {
            lastIndex = index;
            return value.key === key;
        });
        if (item)
            return { item: item, index: lastIndex };
        else
            return { index: -1 };
    };
    Builder.prototype.clear = function () { this.fields.length = 0; };
    Builder.prototype.resolveProps = function (props) { return props; };
    Builder.prototype.props = function () {
        // @ts-ignore
        if (typeof this._props === "function")
            this._props = this._props();
        if (!this._resolvedProps) {
            this._resolvedProps = true;
            // @ts-ignore
            this._props = this.resolveProps(this._props);
        }
        // @ts-ignore
        return this._props;
    };
    Builder.prototype.onFinallyPublished = function () { };
    ;
    return Builder;
}());
exports.Builder = Builder;
//# sourceMappingURL=builder.js.map