"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PGBuilder = void 0;
var file_system_builder_1 = require("./file-system.builder");
var types_1 = require("zoo.pg/lib/types");
var PGBuilder = /** @class */ (function (_super) {
    __extends(PGBuilder, _super);
    function PGBuilder(args) {
        var _this = _super.call(this, args) || this;
        _this._createFunction = function () {
            var regClass = _this.info().regClass;
            //language=PostgreSQL
            return "create or replace function " + regClass + "() returns " + regClass + " language sql as ' select * from " + regClass + "; '";
        };
        _this.languageName = "PGBuilder";
        return _this;
    }
    PGBuilder.prototype.info = function () {
        var schemaName = this.props().schema && this.props().schema.length > 0 ? JSON.stringify(this.props().schema.toString()) : null;
        var tableName = JSON.stringify(this.props().table.toString());
        var regClass = schemaName ? schemaName + "." + tableName : tableName;
        return { schemaName: schemaName, tableName: tableName, regClass: regClass };
    };
    PGBuilder.prototype.commands = function () {
        function sql(strings) {
            var values = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                values[_i - 1] = arguments[_i];
            }
            var sql = "";
            strings.forEach(function (value, index) {
                sql += strings[index];
                if (index < values.length)
                    sql += values[index];
            });
            return sql;
        }
        var _a = this.info(), regClass = _a.regClass, tableName = _a.tableName, schemaName = _a.schemaName;
        var commands = [];
        commands.push({ command: sql(templateObject_1 || (templateObject_1 = __makeTemplateObject(["-- setups"], ["-- setups"]))), type: "comment" });
        commands.push({ command: sql(templateObject_2 || (templateObject_2 = __makeTemplateObject(["create schema if not exists  ", ""], ["create schema if not exists  ", ""])), schemaName), type: "sql" });
        commands.push({ command: sql(templateObject_3 || (templateObject_3 = __makeTemplateObject(["drop function if exists ", "()"], ["drop function if exists ", "()"])), regClass), type: "sql" });
        if (this.props().rebuild)
            commands.push({ command: sql(templateObject_4 || (templateObject_4 = __makeTemplateObject(["drop table if exists ", ""], ["drop table if exists ", ""])), regClass), type: "sql" });
        commands.push({ command: sql(templateObject_5 || (templateObject_5 = __makeTemplateObject(["create table if not exists  ", "()"], ["create table if not exists  ", "()"])), regClass), type: "sql" });
        var allKeys = [];
        this.fields.forEach(function (value) { return allKeys.push(value.key); });
        commands.push({});
        commands.push({ command: sql(templateObject_6 || (templateObject_6 = __makeTemplateObject(["-- compile columns ", ""], ["-- compile columns ", ""])), allKeys.join(', ')), type: "comment" });
        this.fields.forEach(function (_a) {
            var value = _a.value, key = _a.key;
            var _b = types_1.Types.__recognize(value), literal = _b.literal, type = _b.type;
            key = JSON.stringify(key);
            var val = literal;
            commands.push({});
            commands.push({ command: sql(templateObject_7 || (templateObject_7 = __makeTemplateObject(["-- ", " ", ""], ["-- ", " ", ""])), key, value) });
            commands.push({ command: sql(templateObject_8 || (templateObject_8 = __makeTemplateObject(["alter table ", " drop column if exists ", ""], ["alter table ", " drop column if exists ", ""])), regClass, key), type: "sql" });
            commands.push({ command: sql(templateObject_9 || (templateObject_9 = __makeTemplateObject(["alter table ", " add column ", " ", " default ", ""], ["alter table ", " add column ", " ", " default ", ""])), regClass, key, type, val),
                alternative: sql(templateObject_10 || (templateObject_10 = __makeTemplateObject(["alter table ", " alter column ", " type ", " using ", "; alter table ", " alter column ", " set default ", ""], ["alter table ", " alter column ", " type ", " using ", "; alter table ", " alter column ", " set default ", ""])), regClass, key, type, val, regClass, key, val), type: "sql" });
        });
        commands.push({});
        commands.push({ command: sql(templateObject_11 || (templateObject_11 = __makeTemplateObject(["--start cont values"], ["--start cont values"]))) });
        commands.push({ command: sql(templateObject_12 || (templateObject_12 = __makeTemplateObject(["delete from  ", " where true"], ["delete from  ", " where true"])), regClass), type: "sql" });
        commands.push({ command: sql(templateObject_13 || (templateObject_13 = __makeTemplateObject(["insert into ", " default values"], ["insert into ", " default values"])), regClass), type: "sql" });
        commands.push({});
        commands.push({ command: sql(templateObject_14 || (templateObject_14 = __makeTemplateObject(["--create function"], ["--create function"]))) });
        commands.push({ command: this._createFunction(), type: "sql" });
        return commands;
    };
    PGBuilder.prototype.structure = function () {
        var sql = "";
        var _a = this.info(), regClass = _a.regClass, tableName = _a.tableName, schemaName = _a.schemaName;
        var commands = this.commands();
        sql += "do $$\n";
        sql += "  begin\n";
        commands.forEach(function (value, index) {
            sql += "    " + (value.command || '') + (value.type === "sql" ? ";" : "") + "\n";
        });
        sql += "  end\n";
        sql += "$$;\n";
        return sql;
    };
    PGBuilder.prototype.save = function () {
        var fileName = this.props().schema + "." + this.props().table + ".sql";
        return _super.prototype.saveFile.call(this, {
            dir: this.props().dir,
            fileName: fileName
        });
    };
    PGBuilder.prototype.getPoll = function () {
        if (!this._pool)
            this._pool = this.props().pool();
        return this._pool;
    };
    PGBuilder.prototype.onFinallyPublished = function () {
        var _this = this;
        var self = this;
        _super.prototype.onFinallyPublished.call(this);
        if (!this.getPoll())
            throw new Error("Function not returned a pool");
        if (this.props().breakOnError) {
            console.log("importing const to database...");
            this.getPoll().query(this.structure()).then(function (value) {
                console.log("importing const to database... ok!");
                _this.getPoll().end();
            }).catch(function (reason) {
                console.log("importing const to database... failed!");
                console.error(reason);
                _this.getPoll().end();
            });
        }
        else {
            var commands = this.commands().filter(function (value) { return value.type === "sql"; });
            var regPromise_1 = function (sql, alternative, isAlternative, original) {
                return new Promise(function (resolve) {
                    var name = "executing";
                    if (isAlternative) {
                        name = "executing:alternative";
                        console.log("alternative of", original);
                    }
                    var result;
                    _this.getPoll().query(sql)
                        .then(function () {
                        console.log(name + " " + sql + "... ok!");
                        result = true;
                    })
                        .catch(function () {
                        console.log(name + " " + sql + "... failed!");
                        if (!isAlternative && alternative)
                            return regPromise_1(alternative, null, true, sql);
                    }).finally(function () {
                        resolve(result);
                    });
                });
            };
            var retry = function (next) {
                try {
                    return regPromise_1(next.command, next.alternative, false);
                }
                catch (e) {
                    if (self.props().showError)
                        console.error("error", [next.command]);
                    return new Promise(function (resolve) { return resolve(false); });
                }
            };
            for (var i = 0; i < commands.length; i++) {
                retry(commands[i]);
            }
            this.getPoll().end();
        }
    };
    PGBuilder.JAVA_MAP = {
        boolean: "boolean",
        byte: "smallint",
        short: "smallint",
        int: "integer",
        long: "bigint",
        float: "float",
        double: "double precision",
        String: "varchar",
    };
    return PGBuilder;
}(file_system_builder_1.FileSystemBuilder));
exports.PGBuilder = PGBuilder;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14;
//# sourceMappingURL=pg.builder.js.map