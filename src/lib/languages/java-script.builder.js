"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.JavaScriptBuilder = void 0;
var file_system_builder_1 = require("./file-system.builder");
var JavaScriptBuilder = /** @class */ (function (_super) {
    __extends(JavaScriptBuilder, _super);
    function JavaScriptBuilder(args) {
        var _this = _super.call(this, args) || this;
        _this.languageName = "JavaScriptBuilder";
        return _this;
    }
    JavaScriptBuilder.prototype.structure = function () {
        var _this = this;
        var text = "";
        var i = 0;
        this.fields.forEach(function (_a) {
            var key = _a.key, value = _a.value;
            var tNext = "    " + key + ": " + JSON.stringify(value);
            if (++i < _this.fields.length)
                tNext += ",";
            text += "\n" + tNext;
        });
        return "const " + this.props().className + " = {\n" + text + "\n\n};";
    };
    ;
    JavaScriptBuilder.prototype.extension = function () { return '.js'; };
    JavaScriptBuilder.prototype.save = function () {
        var fullPath;
        if (this.props().dir)
            fullPath = "" + this.props().dir;
        return this.saveFile({
            dir: this.props().dir,
            fileName: "" + this.props().className + this.extension(),
        });
    };
    ;
    return JavaScriptBuilder;
}(file_system_builder_1.FileSystemBuilder));
exports.JavaScriptBuilder = JavaScriptBuilder;
//# sourceMappingURL=java-script.builder.js.map