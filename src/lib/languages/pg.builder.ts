import {FileSystemBuilder} from "./file-system.builder";
import {Pool} from "pg";
import {Props} from "./builder";
import {Types} from "zoo.pg/lib/types";


export type PGProps  = Props & {
    schema:string,
    table:string,
    dir:string,
    pool: () => Pool,
    rebuild,
    breakOnError,
    showError
};
export type PGArgs = PGProps | ( ()=> PGProps );

export type Command = { command?:string, alternative?, type?:"sql"|"comment" };

export class PGBuilder< P extends PGProps, A extends PGArgs> extends  FileSystemBuilder< P, A > {

    static JAVA_MAP = {
        boolean: "boolean",
        byte: "smallint",
        short: "smallint",
        int: "integer",
        long: "bigint",
        float: "float",
        double: "double precision",
        String: "varchar",
    };
    
    private _pool: Pool;

    constructor( args: A ) {
        super( args );
        this.languageName = "PGBuilder";
    }

    info(){
        let schemaName = this.props().schema && this.props().schema.length > 0 ? JSON.stringify( this.props().schema.toString() ) : null;
        let tableName = JSON.stringify( this.props().table.toString() );
        let regClass = schemaName? `${ schemaName }.${ tableName }` : tableName;
        return { schemaName, tableName, regClass };
    }

    commands(): Command[]{

        function sql( strings, ...values ){
            let sql = "";
            strings.forEach( ( value, index) => {
                sql+= strings[ index ];
                if( index < values.length ) sql += values[ index ];
            });
            return sql;
        }

        const { regClass, tableName, schemaName } = this.info();

        const commands:Command[] = [];


        commands.push( { command: sql`-- setups`, type: "comment" } );
        commands.push( { command: sql`create schema if not exists  ${ schemaName }`, type: "sql" } );
        commands.push( { command: sql`drop function if exists ${ regClass }()`, type: "sql" } );
        if( this.props().rebuild ) commands.push( { command: sql`drop table if exists ${ regClass }`, type: "sql" } );
        commands.push( { command: sql`create table if not exists  ${ regClass }()`, type: "sql" } );

        const allKeys = [];
        this.fields.forEach( value => allKeys.push( value.key ) );

        commands.push( {} );
        commands.push( { command: sql`-- compile columns ${allKeys.join(', ')}`, type: "comment" } );
        this.fields.forEach( ({value, key}) => {

            const { literal, type } = Types.__recognize( value );
            key = JSON.stringify( key );

            const val = literal;
            commands.push( { } );
            commands.push( { command: sql`-- ${key} ${ value }` } );
            commands.push( { command: sql`alter table ${ regClass } drop column if exists ${ key }`, type: "sql" } );
            commands.push( { command: sql`alter table ${ regClass } add column ${ key } ${ type } default ${ val }`,
                alternative:
                    sql`alter table ${ regClass } alter column ${ key } type ${ type } using ${ val }; alter table ${ regClass } alter column ${ key } set default ${ val }`,

                type: "sql"
            });
        });
        commands.push( { } );
        commands.push( { command: sql`--start cont values` } );
        commands.push( { command: sql`delete from  ${ regClass } where true`, type: "sql" } );
        commands.push( { command: sql`insert into ${ regClass } default values`, type: "sql" } );
        commands.push( { } );
        commands.push( { command: sql`--create function` } );
        commands.push( { command: this._createFunction(), type: "sql" })
        return commands;
    }

    structure() {
        let sql = "";
        const { regClass, tableName, schemaName } = this.info();
        const commands = this.commands();
        sql += "do $$\n";
        sql += "  begin\n";

        commands.forEach( (value, index) => {
            sql += `    ${ value.command||'' }${ value.type === "sql"? ";": "" }\n`
        });

        sql += "  end\n";
        sql += "$$;\n";
        return sql;
    }

    save() {
        const fileName = `${ this.props().schema }.${ this.props().table }.sql`;
        return super.saveFile({
            dir: this.props().dir,
            fileName
        })
    }

    private getPoll():Pool{
        if( !this._pool ) this._pool = this.props().pool();
        return this._pool;
    }

    _createFunction= (  )=>{
        const { regClass } = this.info();
        //language=PostgreSQL
        return `create or replace function ${ regClass }() returns ${ regClass } language sql as ' select * from ${ regClass }; '`
    }

    onFinallyPublished() {
        const self = this;

        super.onFinallyPublished();

        if (!this.getPoll()) throw new Error("Function not returned a pool");

        if (this.props().breakOnError) {
            console.log("importing const to database...")
            this.getPoll().query(this.structure()).then(
                value => {
                    console.log("importing const to database... ok!")
                    this.getPoll().end();

                }).catch(reason => {
                console.log("importing const to database... failed!")
                console.error(reason);
                this.getPoll().end();
            })
        } else {
            const commands = this.commands().filter(value => value.type === "sql");

            const regPromise = ( sql:string, alternative, isAlternative, original? ) => {
                return new Promise( resolve => {
                    let name = "executing";
                    if (isAlternative) {
                        name = "executing:alternative";
                        console.log("alternative of", original );
                    }
                    let result;
                    this.getPoll().query(sql)
                        .then(() => {
                            console.log(`${name} ${sql}... ok!`)
                            result = true;
                        })
                        .catch(() => {
                            console.log(`${name} ${sql}... failed!`);
                            if (!isAlternative && alternative)
                                return regPromise( alternative, null, true, sql );
                        }).finally(() => {
                            resolve( result );
                        });
                });
            }


            const retry = function ( next:Command ) {
                try {
                    return regPromise( next.command, next.alternative, false );
                } catch (e) {
                    if (self.props().showError) console.error("error", [next.command]);
                    return new Promise( resolve => resolve( false ) );
                }
            }

            for (let i = 0; i < commands.length; i++) {
                retry(commands[i]);
            }
            this.getPoll().end();
        }

    }
}
