import {FileSystemBuilder} from "./file-system.builder";

export type PHPProps = { className, dir };
export type PHPArgs = PHPProps | ( ()=>PHPProps );
export class PhpBuilder< P extends PHPProps, A extends PHPArgs > extends FileSystemBuilder< P, A>  {

    _super;

    constructor( args: A  ) {
        let _super;
        _super = super( args );
        this._super  = _super;
        this.languageName = "PhpBuilder";
    }

    structure( ):string {
        let text = "";
        let i = 0;
        console.log( this.fields );
        this.fields.forEach( ({key, value}) => {
            let  tNext = `    const ${ key } = ${ JSON.stringify( value ) };`;
            // if( ++i < this.fields.length ) tNext+=";";
            text +="\n"+tNext
        });
        return `<?php\n class ${ this.props().className } {\n${text}\n\n};`;
    };

    save() {
        let fullPath;
        if( this.props().dir ) fullPath = `${ this.props().dir }`;

        return this.saveFile({
            dir: this.props().dir,
            fileName: `${ this.props().className }.php`,
        });
    };
}