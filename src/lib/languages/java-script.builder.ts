import {FileSystemBuilder} from "./file-system.builder";
import {Props} from "./builder";

export type JSProps = Props &  { className: string, dir: string };
export type JSArgs = JSProps | ( ()=>JSProps );

export class JavaScriptBuilder< P extends JSProps, A extends JSArgs > extends FileSystemBuilder< P, A >  {

    constructor( args:A ) {
        super( args )
        this.languageName = "JavaScriptBuilder";
    }

    structure ( ){
        let text = "";
        let i = 0;
        this.fields.forEach( ({key, value}) => {
            let  tNext = `    ${ key }: ${JSON.stringify( value )}`;
            if( ++i < this.fields.length ) tNext+=",";
            text +="\n"+tNext
        });
        return `const ${ this.props().className } = {\n${text}\n\n};`;
    };

    extension() { return '.js' }

    save  () {
        let fullPath;
        if( this.props().dir ) fullPath = `${ this.props().dir }`;

        return this.saveFile({
            dir: this.props().dir,
            fileName: `${ this.props().className }${ this.extension() }`,
        });
    };
}