"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileSystemBuilder = void 0;
var builder_1 = require("./builder");
var shell = require('shelljs');
var fs = require('fs');
/**
 * @abstract
 * @constructor
 * @extends Builder
 */
var FileSystemBuilder = /** @class */ (function (_super) {
    __extends(FileSystemBuilder, _super);
    function FileSystemBuilder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FileSystemBuilder.prototype.saveFile = function (args) {
        if (!fs.existsSync(args.dir)) {
            shell.mkdir('-p', args.dir);
        }
        var path;
        if (args.dir)
            path = args.dir + "/" + args.fileName;
        else
            path = args.fileName;
        console.log("saveFile", path, "language", this.languageName);
        fs.writeFileSync(path, this.structure());
        return {
            fileName: args.fileName,
            path: path,
            dir: args.dir
        };
    };
    return FileSystemBuilder;
}(builder_1.Builder));
exports.FileSystemBuilder = FileSystemBuilder;
//# sourceMappingURL=file-system.builder.js.map