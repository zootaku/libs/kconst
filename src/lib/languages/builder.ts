
export type Props = {};
export type Args = Props | (<P extends Props >()=>P)

export abstract class  Builder< P extends Props, A extends Props > {

    languageName;
    private _props:A;
    private _resolvedProps:boolean;

    constructor( a:A ) {
        this.languageName = "LanguageBuilder";
        this._props = a;
    }

    /**
     * @type {{key:string, value:*}[]}
     */
    fields = [];


    /**
     * @param key
     * @param value
     */
    add ( { key, value } ) {
        const { index } = this.find( key );
        if( index >= 0 ) this.fields[ index ] = { key, value };
        else this.fields.push( { key, value } );
    };

    removeIf( key ){
        const { index } = this.find( key );
        if( index !== -1 ) this.fields.splice( index, 1 );
        return index;
    }

    find( key ) {
        let lastIndex;
        const item = this.fields.find( (value, index) => {
            lastIndex = index;
            return value.key === key;
        });

        if( item ) return { item, index: lastIndex }
        else return { index: -1 };
    }

    clear(){ this.fields.length = 0; }

    protected resolveProps( props:P ):P{ return props}

    protected props():P {

        // @ts-ignore
        if( typeof this._props === "function" ) this._props = this._props();
        if( !this._resolvedProps ){
            this._resolvedProps = true;

            // @ts-ignore
            this._props = this.resolveProps( this._props );
        }

        // @ts-ignore
        return this._props;
    }

    abstract structure ():string;

    /**
     * @abstract
     * @return {*}
     */
    abstract save ():boolean|void|any;

    onFinallyPublished(){ };
}