"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.JavaBuilder = void 0;
var file_system_builder_1 = require("./file-system.builder");
/**
 * @param packageName
 * @param className
 * @param srcDir
 * @constructor
 * @extends FileSystemBuilder
 */
var JavaBuilder = /** @class */ (function (_super) {
    __extends(JavaBuilder, _super);
    function JavaBuilder(args) {
        var _this = _super.call(this, args) || this;
        _this.languageName = "JavaBuilder";
        _this.javaArgs = args;
        return _this;
    }
    JavaBuilder.typeOf = function (value) {
        var _a = JavaBuilder.TYPES, byte = _a.byte, short = _a.short, int = _a.int, long = _a.long, float = _a.float, double = _a.double, String = _a.String;
        if (typeof value === "boolean")
            return { type: "boolean", val: value };
        else if (typeof value === "number" && Number.isInteger(value) && value >= JavaBuilder.MIN_BYTE && value <= JavaBuilder.MAX_BYTE)
            return { type: byte, val: value };
        else if (typeof value === "number" && Number.isInteger(value) && value >= JavaBuilder.MIN_SHORT && value <= JavaBuilder.MAX_SHORT)
            return { type: short, val: value };
        else if (typeof value === "number" && Number.isInteger(value) && value >= JavaBuilder.MIN_INT && value <= JavaBuilder.MAX_INT)
            return { type: int, val: value };
        else if (typeof value === "number" && Number.isInteger(value) && value >= JavaBuilder.MIN_LONG && value <= JavaBuilder.MAX_LONG)
            return { type: long, val: "" + value, representation: "L" };
        else if (typeof value === "number" && value >= JavaBuilder.MIN_FLOAT && value <= JavaBuilder.MAX_FLOAT)
            return { type: float, val: "" + value, representation: "F" };
        else if (typeof value === "number" && value >= JavaBuilder.MIN_DOUBLE && value <= JavaBuilder.MAX_DOUBLE)
            return { type: double, val: "" + value, representation: "D" };
        else if (typeof value === "number" && Number.isInteger(value))
            return { type: "long", val: value };
        else if (typeof value === "number")
            return { type: "double", val: value };
        else
            return { type: String, val: JSON.stringify(value.toString()) };
    };
    JavaBuilder.prototype.resolveProps = function (props) {
        if (props.packageName)
            props.packageName = props.packageName.toLowerCase();
        return props;
    };
    JavaBuilder.prototype.structure = function () {
        var text = "";
        this.fields.forEach(function (_a) {
            var key = _a.key, value = _a.value;
            var _b = JavaBuilder.typeOf(value), type = _b.type, val = _b.val, representation = _b.representation;
            if (!representation)
                representation = "";
            var tNext = "    public static final " + type + " " + key + " = " + val + representation + ";";
            text += "\n" + tNext;
        });
        var jClass = "package " + this.props().packageName + "; \n\n";
        jClass += "public class " + this.props().className + " {\n" + text + "\n\n}";
        return jClass;
    };
    ;
    JavaBuilder.prototype.save = function () {
        console.log("saving to java...");
        var packageDir = this.props().packageName.split(".").join("/");
        var dir;
        if (this.props().dir)
            dir = this.props().dir + "/" + packageDir + "/";
        else
            dir = packageDir;
        var classFile = this.props().className + ".java";
        var saveFile = _super.prototype.saveFile.call(this, {
            dir: dir,
            fileName: classFile
        });
        return {
            packageName: this.props().packageName,
            classFile: saveFile.fileName,
            className: this.props().className
        };
    };
    ;
    JavaBuilder.TYPES = {
        byte: "byte",
        short: "short",
        int: "int",
        long: "long",
        float: "float",
        double: "double",
        String: "String"
    };
    JavaBuilder.MIN_BYTE = -128;
    JavaBuilder.MAX_BYTE = 127;
    JavaBuilder.MAX_SHORT = 32767;
    JavaBuilder.MIN_SHORT = -32768;
    JavaBuilder.MIN_INT = 0x80000000;
    JavaBuilder.MAX_INT = 0x7fffffff;
    JavaBuilder.MIN_LONG = 0x8000000000000000;
    JavaBuilder.MAX_LONG = 0x7fffffffffffffff;
    JavaBuilder.MIN_FLOAT = 1.4e-45;
    JavaBuilder.MAX_FLOAT = 3.4028235e+38;
    JavaBuilder.MIN_DOUBLE = 4.9e-324;
    JavaBuilder.MAX_DOUBLE = 1.7976931348623157e+308;
    return JavaBuilder;
}(file_system_builder_1.FileSystemBuilder));
exports.JavaBuilder = JavaBuilder;
//# sourceMappingURL=java.builder.js.map