import {Args, Builder, Props} from "./builder";

const shell = require( 'shelljs' );
const fs = require('fs');

/**
 * @abstract
 * @constructor
 * @extends Builder
 */
export abstract class FileSystemBuilder< P extends Props, A extends  Args > extends Builder< P, A > {


    saveFile ( args:{ dir?:string, fileName:string } ):{ fileName:string, path:string, dir:string } {

        if( !fs.existsSync( args.dir ) ){
            shell.mkdir( '-p', args.dir );
        }

        let path;
        if( args.dir ) path = `${ args.dir}/${ args.fileName }`;
        else path = args.fileName;

        console.log( "saveFile", path, "language", this.languageName );
        fs.writeFileSync( path, this.structure() );

        return {
            fileName: args.fileName,
            path,
            dir: args.dir
        }
    }
}
