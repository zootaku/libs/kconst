import {JavaScriptBuilder, JSArgs, JSProps} from "./java-script.builder";


export type NodeJSProps = JSProps & { typescript:boolean, className: string, dir: string };
export type NodeJsArgs = NodeJSProps | (()=> NodeJSProps );

export class NodeJSBuilder < P extends NodeJSProps, A extends NodeJsArgs > extends JavaScriptBuilder< P, A > {

    private readonly typescript: boolean
    constructor( args: A ) {
        super ( args );
        this.languageName = "NodeJsBuilder";
        this.typescript = this.props().typescript;
    }

    extension() {
        return this.typescript? '.ts':'.js';
    }

    structure ( ){
        let jClass = super.structure();
        jClass+="\n\n";
        if( !!this.typescript ) jClass += `export { ${ this.props().className } };`;
        else jClass+=`module.exports = { ${ this.props().className } };`;
        return jClass;
    };
}
