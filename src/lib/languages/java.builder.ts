import {FileSystemBuilder} from "./file-system.builder";
import {Props} from "./builder";

export type JavaProps = Props & { className: string, packageName: string, dir:string };
export type JavaArgs = JavaProps | ( ()=>JavaProps );

/**
 * @param packageName
 * @param className
 * @param srcDir
 * @constructor
 * @extends FileSystemBuilder
 */
export class JavaBuilder< P extends JavaProps, A extends JavaArgs > extends FileSystemBuilder< P, A >   {
    static TYPES = {
        byte: "byte",
        short: "short",
        int: "int",
        long: "long",
        float: "float",
        double: "double",
        String: "String"
    };

    static MIN_BYTE = -128;
    static MAX_BYTE = 127;
    static MAX_SHORT = 32767;
    static MIN_SHORT = -32768;
    static MIN_INT = 0x80000000;
    static MAX_INT = 0x7fffffff;
    static MIN_LONG = 0x8000000000000000;
    static MAX_LONG = 0x7fffffffffffffff;
    static MIN_FLOAT = 1.4e-45;
    static MAX_FLOAT = 3.4028235e+38;
    static MIN_DOUBLE = 4.9e-324;
    static MAX_DOUBLE = 1.7976931348623157e+308;

    static typeOf( value ){
        const { byte, short, int, long, float, double, String } = JavaBuilder.TYPES;
        if ( typeof value === "boolean" ) return {type: "boolean", val: value };
        else if ( typeof value === "number" && Number.isInteger( value ) &&  value >= JavaBuilder.MIN_BYTE && value <= JavaBuilder.MAX_BYTE ) return {type: byte, val: value };
        else if ( typeof value === "number" && Number.isInteger( value ) && value >= JavaBuilder.MIN_SHORT && value <= JavaBuilder.MAX_SHORT ) return {type: short, val: value };
        else if ( typeof value === "number" && Number.isInteger( value ) && value >= JavaBuilder.MIN_INT && value <= JavaBuilder.MAX_INT ) return {type: int, val: value };
        else if ( typeof value === "number" && Number.isInteger( value ) && value >= JavaBuilder.MIN_LONG && value <= JavaBuilder.MAX_LONG ) return {type: long, val: `${value}`, representation: "L" };
        else if ( typeof value === "number" && value >= JavaBuilder.MIN_FLOAT && value <= JavaBuilder.MAX_FLOAT ) return {type: float, val: `${value}`, representation: "F" };
        else if ( typeof value === "number" && value >= JavaBuilder.MIN_DOUBLE && value <= JavaBuilder.MAX_DOUBLE ) return {type: double, val: `${value}`, representation: "D" };
        else if( typeof value === "number" && Number.isInteger( value ) ) return { type: "long", val: value};
        else if( typeof value === "number" ) return { type: "double", val: value};
        else return { type: String, val: JSON.stringify( value.toString() )}
    }

    private readonly javaArgs:JavaArgs;


    protected resolveProps( props: P ): P {
        if( props.packageName  ) props.packageName = props.packageName.toLowerCase();
        return props;
    }

    constructor( args:A  ) {
        super( args );
        this.languageName = "JavaBuilder"
        this.javaArgs = args;
    }

    structure ():string {
        let text = "";
        this.fields.forEach( ({key, value}) => {
            let {type, val, representation } = JavaBuilder.typeOf( value );
            if( !representation ) representation ="";
            const  tNext = `    public static final ${type} ${ key } = ${val}${representation};`;
            text +="\n"+tNext
        });
        let jClass = `package ${ this.props().packageName }; \n\n`;
        jClass += `public class ${ this.props().className } {\n${text}\n\n}`;
        return jClass;
    };

    save ( ):boolean|void|any {
        console.log( "saving to java...")
        let packageDir = this.props().packageName.split( "." ).join( "/" );
        let dir;
        if( this.props().dir ) dir = `${ this.props().dir }/${ packageDir }/`;
        else dir = packageDir;
        const classFile = `${ this.props().className }.java`;

        const saveFile = super.saveFile({
            dir: dir,
            fileName: classFile
        });

        return {
            packageName: this.props().packageName,
            classFile: saveFile.fileName,
            className: this.props().className
        }
    };
}

