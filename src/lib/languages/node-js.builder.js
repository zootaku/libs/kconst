"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.NodeJSBuilder = void 0;
var java_script_builder_1 = require("./java-script.builder");
var NodeJSBuilder = /** @class */ (function (_super) {
    __extends(NodeJSBuilder, _super);
    function NodeJSBuilder(args) {
        var _this = _super.call(this, args) || this;
        _this.languageName = "NodeJsBuilder";
        _this.typescript = _this.props().typescript;
        return _this;
    }
    NodeJSBuilder.prototype.extension = function () {
        return this.typescript ? '.ts' : '.js';
    };
    NodeJSBuilder.prototype.structure = function () {
        var jClass = _super.prototype.structure.call(this);
        jClass += "\n\n";
        if (!!this.typescript)
            jClass += "export { " + this.props().className + " };";
        else
            jClass += "module.exports = { " + this.props().className + " };";
        return jClass;
    };
    ;
    return NodeJSBuilder;
}(java_script_builder_1.JavaScriptBuilder));
exports.NodeJSBuilder = NodeJSBuilder;
//# sourceMappingURL=node-js.builder.js.map