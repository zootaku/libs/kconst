"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.KConst = exports.Declaration = exports.TriggerEvent = exports.TriggersMethod = exports.FormatterType = exports.SelectorItem = void 0;
var path = __importStar(require("path"));
var file_util_1 = require("zoo.util/lib/file-util");
var proxy_util_1 = require("zoo.util/lib/proxy-util");
var target_1 = require("./target");
var os = __importStar(require("os"));
var KConstCollector_1 = require("./KConstCollector");
var SelectorItem;
(function (SelectorItem) {
    SelectorItem["MODE"] = "mode";
    SelectorItem["USER"] = "user";
    SelectorItem["MODE_USER"] = "mode_user";
    SelectorItem["DEFAULT"] = "*";
})(SelectorItem = exports.SelectorItem || (exports.SelectorItem = {}));
var DEFAULT_PRIORITY = [SelectorItem.DEFAULT, SelectorItem.MODE, SelectorItem.USER, SelectorItem.MODE_USER];
var FormatterType;
(function (FormatterType) {
    FormatterType["REAL_NAME"] = "REAL_NAME";
    FormatterType["UPPER"] = "UPPER";
    FormatterType["LOWER"] = "LOWER";
})(FormatterType = exports.FormatterType || (exports.FormatterType = {}));
var TriggersMethod;
(function (TriggersMethod) {
    TriggersMethod["DECLARE"] = "initialize";
    TriggersMethod["PUBLISH"] = "publish";
})(TriggersMethod = exports.TriggersMethod || (exports.TriggersMethod = {}));
var TriggerEvent;
(function (TriggerEvent) {
    TriggerEvent["BEFORE"] = "before";
    TriggerEvent["AFTER"] = "after";
})(TriggerEvent = exports.TriggerEvent || (exports.TriggerEvent = {}));
var Callback;
(function (Callback) {
    Callback["DECLARES"] = "declares";
    Callback["TRIGGERS"] = "triggers";
    Callback["EVENTS"] = "events";
    Callback["TARGETS"] = "targets";
})(Callback || (Callback = {}));
var Declaration;
(function (Declaration) {
    Declaration[Declaration["CREATE"] = 0] = "CREATE";
    Declaration[Declaration["OVERRIDE"] = 1] = "OVERRIDE";
})(Declaration = exports.Declaration || (exports.Declaration = {}));
var KConst = /** @class */ (function () {
    function KConst(configs) {
        var _this = this;
        this._on = proxy_util_1.ProxyUtil.infinityProxyArray();
        this.dirs = {
            root: function () { return _this.configs.root; },
            targets: function () { return path.join(_this.configs.root, _this.configs.targets); },
            exports: function () { return path.join(_this.configs.root, _this.configs.exports); },
            triggers: function () { return path.join(_this.configs.root, _this.configs.triggers); },
            events: function () { return path.join(_this.configs.root, _this.configs.events); },
        };
        this.configs = {
            formatter: configs.formatter || FormatterType.REAL_NAME,
            mode: configs.mode,
            user: configs.user || os.userInfo().username,
            name: configs.name || "K",
            root: configs.root,
            targets: configs.targets || "targets",
            exports: configs.exports || "exports",
            triggers: configs.triggers || "triggers",
            events: configs.triggers || "events",
            priorityOrder: configs.priorityOrder
        };
        if (!this.configs.priorityOrder)
            this.configs.priorityOrder = [];
        else if (!Array.isArray(this.configs.priorityOrder))
            this.configs.priorityOrder = [this.configs.priorityOrder];
        DEFAULT_PRIORITY.forEach(function (value, index) {
            if (!_this.configs.priorityOrder.includes(value) && Array.isArray(_this.configs.priorityOrder))
                _this.configs.priorityOrder.push(value);
        });
        this.configs.priorityOrder = this.configs.priorityOrder.filter(function (value, index, array) { return array.indexOf(value) === index; });
        this._exportator = new KConstCollector_1.KConstCollector(function (key, value, replace) {
            _this.executeEvents(key, value, replace);
            return true;
        });
        this._target = new target_1.Target();
    }
    Object.defineProperty(KConst.prototype, "exports", {
        get: function () { return this._exports; },
        enumerable: false,
        configurable: true
    });
    KConst.prototype._acceptSelector = function (selector) {
        var mode, user;
        var rejection = [];
        if (selector && typeof selector.mode === "string" && selector.mode.trim().length === 0)
            selector.mode = null;
        if (selector && typeof selector.user === "string" && selector.user.trim().length === 0)
            selector.user = null;
        if (selector && selector.mode && Array.isArray(selector.mode))
            mode = selector.mode;
        else if (selector && selector.mode && !Array.isArray(selector.mode))
            mode = [selector.mode];
        if (selector && selector.user && Array.isArray(selector.user))
            user = selector.user;
        else if (selector && selector.user && !Array.isArray(selector.user))
            user = [selector.user];
        if (mode && mode.length > 0 && !mode.includes(this.configs.mode))
            rejection.push("mode");
        if (user && user.length > 0 && !user.includes(this.configs.user))
            rejection.push("user");
        return rejection.length === 0;
    };
    KConst.prototype.target = function (callback, selector) {
        var collector = this._selectedGroup(Callback.TARGETS, selector);
        if (collector)
            collector.push(callback);
    };
    KConst.prototype._selectedGroup = function (group, selector) {
        if (this._acceptSelector(selector)) {
            if (selector && selector.user && selector.mode)
                return this._on[group][SelectorItem.MODE_USER];
            else if (selector && selector.mode)
                return this._on[group][SelectorItem.MODE];
            else if (selector && selector.user)
                return this._on[group][SelectorItem.USER];
            else
                return this._on[group][SelectorItem.DEFAULT];
        }
        else
            return false;
    };
    KConst.prototype.trigger = function (method, event, callback, selector) {
        var collector = this._selectedGroup(Callback.TRIGGERS, selector);
        if (collector)
            collector[method][event].push(callback);
    };
    KConst.prototype.declares = function (callback, selector) {
        var collector = this._selectedGroup(Callback.DECLARES, selector);
        if (collector)
            collector.push(callback);
    };
    KConst.prototype.on = function (callback, selector) {
        var collector = this._selectedGroup(Callback.EVENTS, selector);
        var applyOn = [];
        if (selector && selector.applyOn && typeof selector.applyOn === "string")
            applyOn.push(selector.applyOn);
        else if (selector && Array.isArray(selector.applyOn))
            applyOn.push.apply(applyOn, selector.applyOn);
        if (collector && applyOn.length > 0)
            applyOn.forEach(function (onKConst) { return collector["/"][onKConst].push(callback); });
        else if (collector && typeof callback === "function")
            collector["*"].push(callback);
    };
    KConst.prototype.createTargets = function () {
        var _this = this;
        console.log("execute targets..");
        var targets = this._priorityList(Callback.TARGETS);
        targets.forEach(function (value) { return value(_this._target); });
        console.log("execute targets... ok!");
    };
    KConst.prototype.executeTriggers = function (method, event) {
        console.log("execute triggers " + event + " " + method + "...");
        var callbacks = this._priorityList(Callback.TRIGGERS);
        var triggers = callbacks[method][event];
        triggers.forEach(function (value) { return value(); });
        console.log("execute triggers " + event + " " + method + "... ok!");
    };
    KConst.prototype._priorityList = function (group) {
        var _this = this;
        var priority = proxy_util_1.ProxyUtil.infinityProxyArray();
        this.configs.priorityOrder.forEach(function (value, index, array) {
            priority.push.apply(priority, _this._on[group][value]);
        });
        return priority;
    };
    KConst.prototype._priorityGroup = function (group) {
        var _this = this;
        var priority = proxy_util_1.ProxyUtil.infinityProxyArray();
        this.configs.priorityOrder.forEach(function (value, index, array) {
            priority.push(_this._on[group][value]);
        });
        return priority;
    };
    KConst.prototype.executeDeclarations = function () {
        var _this = this;
        console.log("execute declarations...");
        var declarations = this._priorityList(Callback.DECLARES);
        declarations.forEach(function (value) {
            _this._exportator.props.restrictBlock();
            value(_this._exportator.props.exports, _this._exportator.props.override, KConstCollector_1.SELF_NAME, _this._exportator.props);
            _this._exportator.props.restrictBlock();
        });
        console.log("execute declarations... ok!");
    };
    KConst.prototype.executeEvents = function (key, value, replace) {
        console.log("execute declarations...");
        var events = this._priorityGroup(Callback.EVENTS);
        var allEvents = [];
        allEvents.push.apply(allEvents, (events["/"][key]));
        allEvents.push.apply(allEvents, events["*"]);
        for (var i = 0; i < allEvents.length; i++) {
            if (allEvents[i](key, value, replace))
                break;
        }
        console.log("execute declarations... ok!");
    };
    Object.defineProperty(KConst.prototype, "K", {
        get: function () {
            return this._exportator.K;
        },
        enumerable: false,
        configurable: true
    });
    KConst.prototype.collectCallbacks = function () {
        var collections = [
            { name: "target", group: "targets", filter: /.*.target.js$/, dir: this.dirs.targets() },
            { name: "trigger", group: "triggers", filter: /.*.trigger.js$/, dir: this.dirs.triggers() },
            { name: "export", group: "exports", filter: /.*.export.js$/, dir: this.dirs.exports() },
            { name: "event", group: "events", filter: /.*.event.js$/, dir: this.dirs.triggers() },
        ];
        console.log("loading [*.kconst.js]...");
        file_util_1.FileUtil.loadAllFiles(this.dirs.root(), /.*.kconst.js$/, function (fileRef) {
            require(fileRef.path);
            console.log("kconst: " + fileRef.relative + " [loaded]");
        }, { recursive: true });
        console.log("loading [*.kconst.js]... ok!");
        collections.forEach(function (value, index) {
            console.log("loading [ " + value.group + "]...");
            file_util_1.FileUtil.loadAllFiles(value.dir, value.filter, function (fileRef) {
                require(fileRef.path);
                console.log("trigger: " + fileRef.relative + " [loaded]");
            }, { recursive: true });
            console.log("loading [ " + value.group + "]... ok!");
        });
    };
    KConst.prototype.startDeclarations = function () {
        console.log("initializing...");
        this.createTargets();
        this.executeTriggers(TriggersMethod.DECLARE, TriggerEvent.BEFORE);
        this.executeDeclarations();
        this.executeTriggers(TriggersMethod.DECLARE, TriggerEvent.AFTER);
        console.log("initializing... ok!");
    };
    KConst.prototype.publish = function () {
        console.log("publishing...");
        this.executeTriggers(TriggersMethod.PUBLISH, TriggerEvent.BEFORE);
        this._exportator.publish();
        this.executeTriggers(TriggersMethod.PUBLISH, TriggerEvent.AFTER);
        console.log("publishing... ok!");
    };
    return KConst;
}());
exports.KConst = KConst;
//# sourceMappingURL=KConst.js.map