import {Args, Builder, Props} from "./languages/builder";

class SelfName{
    get code(){ return 982145; }
}

export const SELF_NAME = new SelfName();
export type OnSet = (key:string|number|symbol, value:any, oldValue, replace:boolean )=>boolean|void;
export type Override = ( replacer: () => void )=>void;

export type CollectorProps = {
    K: any,
    exports ( ...builders:Builder<Props, Args>[] );
    exportsAll ( kv, ...builders );
    overrideBlock():void;
    restrictBlock():void;
    override( replacer:()=> void ) ;
}

export class KConstCollector {

    builders:Builder<Props, Args>[] = [];
    private _lastField;
    private _lastBuilders:Builder<Props, Args>[];
    private _on:OnSet;

    private readonly _exportsProps: CollectorProps
    private readonly _KConst;

    private  lastCollected(){ return this._KConst[ this._lastField ]}

    public get K():any{  return this._KConst; }

    constructor( callback?:OnSet ) {
        const self = this;
        this._on = callback;
        function setLastField( p ){
            self._lastField = p;
        }

        const translate = ( object ) => {
            const translated = {};
            Object.keys( object ).forEach( key => {
                let value = object[ key ];
                if( value === SELF_NAME ) value =  key;
                else if( typeof value === "object" && !Array.isArray( value ) )  value = translate( value );
                translated[ key ] = value;
            });
            return translated;
        }

        let can:{ override?:boolean, overrideBlock?:boolean  } = { };

        const canOverride= ( target:any, p:number|string|symbol ) => {
            return can.override || can.overrideBlock;
        }

        this._KConst = new Proxy({}, {
            get(target, p) {
                let value = target[p];
                if (value === SELF_NAME) value = p;
                else if (typeof value === "object" && !Array.isArray(value)) value = translate(value);
                return value;
            },
            // @ts-ignore
            set(target, p, value) {
                const oldValue = target[ p ];
                // @ts-ignore
                if (canOverride(target, p) || Object.keys(target).indexOf(p) === -1) {
                    target[p] = value;
                    self._on(p, value, oldValue, canOverride(target, p));
                    setLastField(p);
                    return true;
                } else {
                    // @ts-ignore
                    throw new Error(`const ${p} already exported`);
                }
            }
        });

        const props = {
            K: self.K,
            exportsAll ( kv, ...builders ){
                if( !kv ) throw new Error( "kv is not object" );
                if( typeof kv !== "object" ) throw new Error( "kv is not object" );
                Object.keys( kv ).forEach( (value, index) => {
                    this.K[ value ] = kv[ value ];
                    props.exports( ...builders );
                });
            },

            exports ( ...builders:Builder<Props, Args>[] )  {

                self._lastBuilders = builders;
                builders.forEach( next => {
                    if( !next ) return;
                    if( self.builders.indexOf( next ) === -1 ) self.builders.push( next );
                    next.add( {  key: self._lastField, value: self.lastCollected() })
                });
            },
            override(replacer: () => void) {
                if( typeof replacer !== "function" ) throw new Error( "replacer is not function" );
                can.override = true;
                replacer();
                self.builders.forEach( next => next.removeIf( self._lastField ) );
                props.exports( ...self._lastBuilders );
                can.override = false;
            },
            overrideBlock() { can.overrideBlock = true; },
            restrictBlock() { can.overrideBlock = false; }
        }

        this._exportsProps = props;

    }

    get props():CollectorProps {
        return this._exportsProps;
    }

    publish ( ...builders:Builder<Props, Args>[] ){
        if( !builders || builders.length === 0 ) builders = this.builders;
        for (let i =0; i< builders.length; i++ ){
            builders[ i ].save();
        }

        for (let i =0; i< builders.length; i++ ){
            builders[ i ].onFinallyPublished();
        }
    }
}