"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Target = void 0;
var java_builder_1 = require("./languages/java.builder");
var java_script_builder_1 = require("./languages/java-script.builder");
var node_js_builder_1 = require("./languages/node-js.builder");
var pg_builder_1 = require("./languages/pg.builder");
var php_builder_1 = require("./languages/php.builder");
var Target = /** @class */ (function () {
    function Target() {
        this._target = [];
    }
    Target.prototype.java = function (props) {
        var builder = new java_builder_1.JavaBuilder(props);
        this._target.push(builder);
        return builder;
    };
    Target.prototype.js = function (props) {
        var builder = new java_script_builder_1.JavaScriptBuilder(props);
        this._target.push(builder);
        return builder;
    };
    Target.prototype.nodeJs = function (props) {
        var builder = new node_js_builder_1.NodeJSBuilder(props);
        this._target.push(builder);
        return builder;
    };
    Target.prototype.pg = function (props) {
        var builder = new pg_builder_1.PGBuilder(props);
        this._target.push(builder);
        return builder;
    };
    Target.prototype.php = function (props) {
        var builder = new php_builder_1.PhpBuilder(props);
        this._target.push(builder);
        return builder;
    };
    return Target;
}());
exports.Target = Target;
//# sourceMappingURL=target.js.map