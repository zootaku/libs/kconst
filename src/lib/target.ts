import {JavaArgs, JavaBuilder, JavaProps} from "./languages/java.builder";
import {Args, Builder, Props} from "./languages/builder";
import {JavaScriptBuilder, JSArgs, JSProps} from "./languages/java-script.builder";
import {NodeJsArgs, NodeJSBuilder, NodeJSProps} from "./languages/node-js.builder";
import {PGArgs, PGBuilder, PGProps} from "./languages/pg.builder";
import {PHPArgs, PhpBuilder, PHPProps} from "./languages/php.builder";

export class Target {

    private _target:Builder<Props, Args>[] = [];

    java( props: JavaArgs ): JavaBuilder< JavaProps, JavaArgs > {
        const builder = new JavaBuilder( props );
        this._target.push( builder )
        return  builder;
    }

    js( props: JSArgs ): JavaScriptBuilder< JSProps, JSArgs> {
        const builder = new JavaScriptBuilder( props );
        this._target.push( builder )
        return  builder;
    }

    nodeJs( props: NodeJsArgs ): NodeJSBuilder< NodeJSProps, NodeJsArgs > {
        const builder = new NodeJSBuilder( props );
        this._target.push( builder )
        return  builder;
    }

    pg( props: PGArgs ): PGBuilder< PGProps, PGArgs > {
        const builder = new PGBuilder( props );
        this._target.push( builder )
        return  builder;
    }

    php( props: PHPArgs ): PhpBuilder< PHPProps, PHPArgs > {
        const builder = new PhpBuilder( props );
        this._target.push( builder )
        return  builder;
    }
}