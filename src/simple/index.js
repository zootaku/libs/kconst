"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.K = exports.kconst = exports.targets = void 0;
require('source-map-support').install();
var arguments_1 = require("zoo.util/lib/arguments");
var os = __importStar(require("os"));
var kconst_1 = require("kconst");
var args = new arguments_1.Arguments(true);
args.argument = { name: "user", alias: "u", value: os.userInfo().username, type: String };
args.argument = { name: "mode", alias: "m", value: "dev" };
var _a = args.values, user = _a.user, mode = _a.mode;
exports.targets = {};
exports.kconst = new kconst_1.KConst({
    root: __dirname,
    user: user,
    mode: mode,
    name: "K",
    targets: "targets",
    triggers: "triggers",
    exports: "exports",
    events: "events",
    priorityOrder: [kconst_1.SelectorItem.USER]
});
exports.K = exports.kconst.K;
(function () {
    exports.kconst.collectCallbacks();
    exports.kconst.startDeclarations();
    exports.kconst.publish();
})();
//# sourceMappingURL=index.js.map