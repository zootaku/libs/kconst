import {K, kconst, targets} from "../../index";

kconst.declares( (exports, override, SELF_NAME, props) => {
    props.overrideBlock();
    K.SIMPLE_CONST = "<<CUSTOM VALUE OF SIMPLE_CONST HERE>>"; exports( targets.simpleNodeFile );
}, { mode: "dev" })


kconst.declares( (exports, override, SELF_NAME, props) => {

    props.override( () => K.SIMPLE_CONST_2 = "<<CUSTOM VALUE OF SIMPLE_CONST 2 HERE>>" );
}, { mode: "dev", label: "K.SIMPLE_CONST_2" });