"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("../../index");
index_1.kconst.declares(function (exports, override, SELF_NAME, props) {
    props.overrideBlock();
    index_1.K.SIMPLE_CONST = "<<CUSTOM VALUE OF SIMPLE_CONST HERE>>";
    exports(index_1.targets.simpleNodeFile);
}, { mode: "dev" });
index_1.kconst.declares(function (exports, override, SELF_NAME, props) {
    props.override(function () { return index_1.K.SIMPLE_CONST_2 = "<<CUSTOM VALUE OF SIMPLE_CONST 2 HERE>>"; });
}, { mode: "dev", label: "K.SIMPLE_CONST_2" });
//# sourceMappingURL=customs.export.js.map