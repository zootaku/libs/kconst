require( 'source-map-support' ).install();

import {Arguments} from "zoo.util/lib/arguments";
import * as os from "os";
import { KConst, SelectorItem } from "kconst";


const args = new Arguments<{ user:string, mode:string }>( true );
args.argument = { name: "user", alias: "u", value: os.userInfo().username, type: String };
args.argument = { name: "mode", alias: "m", value: "dev" };
const { user, mode } = args.values;

export const targets:any = {};

export const kconst = new KConst({
    root: __dirname,
    user,
    mode,
    name: "K",
    targets: "targets",
    triggers: "triggers",
    exports: "exports",
    events: "events",
    priorityOrder: [ SelectorItem.USER ]
});

export const K = kconst.K;

( ()=>{
    kconst.collectCallbacks();
    kconst.startDeclarations();
    kconst.publish();
})()