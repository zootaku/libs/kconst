do $$
    begin
        begin create table if not exists  "public"."K"(); exception when others then null; end;

        -- "CONST_TYPE" = type
        begin alter table  "public"."K" drop column if exists "CONST_TYPE"; exception when others then null; end;
        begin alter table  "public"."K" add column if not exists "CONST_TYPE" varchar default $value$type$value$; exception when others then null; end;

        -- "CONST_TYPE_PG" = pg
        begin alter table  "public"."K" drop column if exists "CONST_TYPE_PG"; exception when others then null; end;
        begin alter table  "public"."K" add column if not exists "CONST_TYPE_PG" varchar default $value$pg$value$; exception when others then null; end;

        -- "CONST_OBJ" = [object Object]
        begin alter table  "public"."K" drop column if exists "CONST_OBJ"; exception when others then null; end;
        begin alter table  "public"."K" add column if not exists "CONST_OBJ" varchar default $value$[object Object]$value$; exception when others then null; end;

        -- "PG_CONST_1" = 1
        begin alter table  "public"."K" drop column if exists "PG_CONST_1"; exception when others then null; end;
        begin alter table  "public"."K" add column if not exists "PG_CONST_1" smallint default 1; exception when others then null; end;

        -- "PG_CONST_2" = 2
        begin alter table  "public"."K" drop column if exists "PG_CONST_2"; exception when others then null; end;
        begin alter table  "public"."K" add column if not exists "PG_CONST_2" smallint default 2; exception when others then null; end;

        -- "PG_CONST_3" = 2
        begin alter table  "public"."K" drop column if exists "PG_CONST_3"; exception when others then null; end;
        begin alter table  "public"."K" add column if not exists "PG_CONST_3" smallint default 2; exception when others then null; end;

        begin delete from  "public"."K" where true; exception when others then null; end;
        begin insert into "public"."K" ( "CONST_TYPE", "CONST_TYPE_PG", "CONST_OBJ", "PG_CONST_1", "PG_CONST_2", "PG_CONST_3" ) values( default, default, default, default, default, default ); exception when others then null; end;
    end
$$;
