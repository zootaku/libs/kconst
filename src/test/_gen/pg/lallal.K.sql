do $$
  begin
    -- setups
    create schema if not exists  "lallal";
    drop function if exists "lallal"."K"();
    drop table if exists "lallal"."K";
    create table if not exists  "lallal"."K"();
    
    -- compile columns CONST_OBJ
    
    -- "CONST_OBJ" [object Object]
    alter table "lallal"."K" drop column if exists "CONST_OBJ";
    alter table "lallal"."K" add column "CONST_OBJ" jsonb default E'{\"name\":7637}'::jsonb;
    
    --start cont values
    delete from  "lallal"."K" where true;
    insert into "lallal"."K" default values;
    
    --create function
    create or replace function "lallal"."K"() returns "lallal"."K" language sql as ' select * from "lallal"."K"; ';
  end
$$;
