do $$
  begin
    -- setups
    create schema if not exists  "gaon";
    drop table if exists "gaon"."k";
    create table if not exists  "gaon"."k"();
    
    -- compile columns category_state_active, category_state_disable, tag_state_active, tag_state_disable, news_state_active, news_state_disable, content_state_active, content_state_complete, content_state_disable, content_progress_active, content_progress_complete, tcontent_content, tcontent_project, category_new, category_project, category_content
    
    -- "category_state_active" 1
    alter table "gaon"."k" drop column if exists "category_state_active";
    alter table "gaon"."k" add column "category_state_active" smallint default 1::smallint;
    
    -- "category_state_disable" -1
    alter table "gaon"."k" drop column if exists "category_state_disable";
    alter table "gaon"."k" add column "category_state_disable" smallint default -1::smallint;
    
    -- "tag_state_active" 1
    alter table "gaon"."k" drop column if exists "tag_state_active";
    alter table "gaon"."k" add column "tag_state_active" smallint default 1::smallint;
    
    -- "tag_state_disable" -1
    alter table "gaon"."k" drop column if exists "tag_state_disable";
    alter table "gaon"."k" add column "tag_state_disable" smallint default -1::smallint;
    
    -- "news_state_active" 1
    alter table "gaon"."k" drop column if exists "news_state_active";
    alter table "gaon"."k" add column "news_state_active" smallint default 1::smallint;
    
    -- "news_state_disable" -1
    alter table "gaon"."k" drop column if exists "news_state_disable";
    alter table "gaon"."k" add column "news_state_disable" smallint default -1::smallint;
    
    -- "content_state_active" 1
    alter table "gaon"."k" drop column if exists "content_state_active";
    alter table "gaon"."k" add column "content_state_active" smallint default 1::smallint;
    
    -- "content_state_complete" 0
    alter table "gaon"."k" drop column if exists "content_state_complete";
    alter table "gaon"."k" add column "content_state_complete" smallint default 0::smallint;
    
    -- "content_state_disable" -1
    alter table "gaon"."k" drop column if exists "content_state_disable";
    alter table "gaon"."k" add column "content_state_disable" smallint default -1::smallint;
    
    -- "content_progress_active" 1
    alter table "gaon"."k" drop column if exists "content_progress_active";
    alter table "gaon"."k" add column "content_progress_active" smallint default 1::smallint;
    
    -- "content_progress_complete" 0
    alter table "gaon"."k" drop column if exists "content_progress_complete";
    alter table "gaon"."k" add column "content_progress_complete" smallint default 0::smallint;
    
    -- "tcontent_content" 1
    alter table "gaon"."k" drop column if exists "tcontent_content";
    alter table "gaon"."k" add column "tcontent_content" smallint default 1::smallint;
    
    -- "tcontent_project" 2
    alter table "gaon"."k" drop column if exists "tcontent_project";
    alter table "gaon"."k" add column "tcontent_project" smallint default 2::smallint;
    
    -- "category_new" 1
    alter table "gaon"."k" drop column if exists "category_new";
    alter table "gaon"."k" add column "category_new" smallint default 1::smallint;
    
    -- "category_project" 2
    alter table "gaon"."k" drop column if exists "category_project";
    alter table "gaon"."k" add column "category_project" smallint default 2::smallint;
    
    -- "category_content" 3
    alter table "gaon"."k" drop column if exists "category_content";
    alter table "gaon"."k" add column "category_content" smallint default 3::smallint;
    
    --start cont values
    delete from  "gaon"."k" where true;
    insert into "gaon"."k" default values;
    
    --create function
    create or replace function "gaon"."k"() returns "gaon"."k" language sql as ' select * from "gaon"."k"; ';
  end
$$;
