import { kconst, targets } from "../index";
import * as path from "path";

kconst.target(target => {

    //TODO declares targets here
    targets.simpleNodeFile = target.nodeJs( { className: "K", typescript: true, dir: path.join( __dirname, "../autogen/node" ), })
    targets.simpleJavaFile = target.java( { className: "K", packageName: "com.simple.kconst", dir: path.join( __dirname, "../autogen/node") })

});







