const commandLineUsage = require('command-line-usage')

const sections = [
    {
        header: 'A typical app',
        content: 'Generates something {italic very} important.'
    },
    {
        header: 'Options',
        optionList: [
            {
                name: 'input',
                typeLabel: '{underline file}',
                description: 'The input to process.'
            },
            {
                name: 'help',
                description: 'Print this usage guide.'
            }
        ]
    },
    {
        header: "Bla Bla Bla",
        content: "Generates something {bold very} important."
    },
    { content: "dsds"},
    { content: "dsds"},
    { content: "dsds"},
]
const usage = commandLineUsage(sections)
console.log(usage)