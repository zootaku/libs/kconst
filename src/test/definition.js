(()=>{

    const { JavaBuilder, JavaScriptBuilder, NodeJSBuilder, PGBuilder, PhpBuilder } = require( '../src/languages.js' );
    const path = require( 'path' );
    const { Pool } = require( 'pg' );

    const pool = ()=> new Pool({
        database: "postgres",
        user: "postgres",
        host: "localhost",
        password: "1234"
    });

    /**
     * @constructor
     */
    const destinations = new ( function Destinations () {
        this.java = new JavaBuilder( { className: "Java", dir: path.resolve(__dirname, "_gen/java" ), classPackage: "java" });
        this.node = new NodeJSBuilder( { className: "Node", dir: path.resolve(__dirname, "_gen/node" ), typescript: true } );
        this.web = new JavaScriptBuilder( { className: "Web", dir: path.resolve(__dirname, "_gen/web" ) } );
        this.php = new PhpBuilder( {className: "PHP", dir: path.resolve(__dirname, "_gen/php" ) } );

        this.pg = new PGBuilder( { schema:"lallal", dir: path.resolve(__dirname, "_gen/pg" ), table: "K", pool, rebuild: true, breakOnError: false });
    });

    const {KConstCollector} = require( '../lib/KConstCollector' );
    const { exports, publish, K } = new KConstCollector( );
    module.exports = { K, exports, publish, destinations };
})();
