(()=>{

    //Global definitions
    const { exports, K, publish, destinations } = require('./definition');
    const { node, web, java, pg, php } = destinations;
    //
    // K.CONST_TYPE = "type"; exports( node, web, java, pg, php );
    // K.CONST_TYPE_NODE = "node"; exports( node );
    // K.CONST_TYPE_WEB = "web"; exports( node, web );
    // K.CONST_TYPE_JAVA = "java"; exports( node, java );
    // K.CONST_TYPE_PG101 = "pg1192"; exports( node, pg );
    // K.CONST_TYPE_PHP = "php"; exports( node, php );

    K.CONST_OBJ = {name: 7637}; exports( node, web, java, pg, php );

    //Load more consts
    const path = require( 'path' );
    const dir = path.join(__dirname, 'definers' );
    const s = require( 'zoo.util/lib/file-util' ).FileUtil.loadAllFiles( dir, /.*.definer.js/, ( file )=>{
        require( file.path );
    }, { recursive: true });

    publish();
})();

