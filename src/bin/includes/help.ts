export const helpText = `
kconst is a utility to initialize kconst config directory.

Usage:
  kconst init [-d consts dir] [OPTIONS]     This command initializes a new 
                                            constant repository 
  kconst init [-d consts dir] [OPTIONS]


Options:
  -h, --help                        Diplay help usage for kconst
  -v, --version                     Display the current version of kconst
init Options:
  -d, --dir                         Directory for kconst
  -b, --base                        Name for folder of kconst in directory
                                    default is "kconst"
  -B, --noBase                      Place necessary kconst files directly 
                                    into the specified directory outside 
                                    your home folder
`;

export function showHelp(){
    console.log( helpText );
}