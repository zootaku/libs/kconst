#!/usr/bin/env node

import {Arguments} from "zoo.util/lib/arguments";
import * as fs from "fs";
import * as path from "path";
import {FileUtil} from "zoo.util/lib/file-util";
import {ProcessTester} from "zoo.util/lib/process-tester";
import {showHelp} from "./includes/help";


const args = new Arguments<{ dir:string, version: boolean, noBase:string, base:string, help:boolean }>( true );
const { ncp } = require( 'ncp' );


args.argument = { name: "dir", value: process.cwd(), type: String, alias: "d" };
args.argument = { name: "version", value: process.cwd(), type: Boolean, alias: "v" };
args.argument = { name: "noBase", value: false, type: Boolean, alias: "B" };
args.argument = { name: "base", value: "kconst", type: String, alias: "b" };
args.define( { name: "help", alias: "h", type: Boolean, value: false } );

if( args.values.help ){
    showHelp();
} else {
    args.defineCommand( { name: [ "create", "init" ], callback: ( { options } ) => {
            //language=file-reference
            FileUtil.loadAllFiles( path.join( __dirname, "../simple" ), /.*.ts$/, _fileRef => {
                let baseDir:string;
                if( options.noBase ) baseDir = options.dir;
                else baseDir = path.join( options.dir, options.base );

                fs.mkdirSync( path.join( baseDir, _fileRef.sub ), { recursive: true } );
                ncp.ncp( path.join( __dirname, "../simple", _fileRef.relative ), path.join( baseDir, _fileRef.relative ),{}, (err )=>{ console.log( err );});
            }, { recursive: true })
        }});

    args.defineCommand( { name: "build", callback: ( { options })=>{
            require( path.resolve( options.dir, "const/index.js" ) );
        }})

    args.defineCommand( ( { options } ) => {
        const pt = new ProcessTester();
        pt.check( args.values.version ).push( ( ) => {
            console.log( require('../../package.json' ).version );
        })
        pt.testFirst();
    });

    args.execute();
}