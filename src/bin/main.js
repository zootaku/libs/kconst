#!/usr/bin/env node
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var arguments_1 = require("zoo.util/lib/arguments");
var fs = __importStar(require("fs"));
var path = __importStar(require("path"));
var file_util_1 = require("zoo.util/lib/file-util");
var process_tester_1 = require("zoo.util/lib/process-tester");
var help_1 = require("./includes/help");
var args = new arguments_1.Arguments(true);
var ncp = require('ncp').ncp;
args.argument = { name: "dir", value: process.cwd(), type: String, alias: "d" };
args.argument = { name: "version", value: process.cwd(), type: Boolean, alias: "v" };
args.argument = { name: "noBase", value: false, type: Boolean, alias: "B" };
args.argument = { name: "base", value: "kconst", type: String, alias: "b" };
args.define({ name: "help", alias: "h", type: Boolean, value: false });
if (args.values.help) {
    help_1.showHelp();
}
else {
    args.defineCommand({ name: ["create", "init"], callback: function (_a) {
            var options = _a.options;
            //language=file-reference
            file_util_1.FileUtil.loadAllFiles(path.join(__dirname, "../simple"), /.*.ts$/, function (_fileRef) {
                var baseDir;
                if (options.noBase)
                    baseDir = options.dir;
                else
                    baseDir = path.join(options.dir, options.base);
                fs.mkdirSync(path.join(baseDir, _fileRef.sub), { recursive: true });
                ncp.ncp(path.join(__dirname, "../simple", _fileRef.relative), path.join(baseDir, _fileRef.relative), {}, function (err) { console.log(err); });
            }, { recursive: true });
        } });
    args.defineCommand({ name: "build", callback: function (_a) {
            var options = _a.options;
            require(path.resolve(options.dir, "const/index.js"));
        } });
    args.defineCommand(function (_a) {
        var options = _a.options;
        var pt = new process_tester_1.ProcessTester();
        pt.check(args.values.version).push(function () {
            console.log(require('../../package.json').version);
        });
        pt.testFirst();
    });
    args.execute();
}
//# sourceMappingURL=main.js.map