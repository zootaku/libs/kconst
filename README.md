# KConst

Simple overview of use/purpose.

## Description

An in-depth paragraph about your project and overview of use.

## Getting Started


### Installing

```
npm install kconst -g
npm link kconst --save-dev
```

### Executing program

* Terminal commands
```
kconst is a utility to initialize kconst config directory.

Usage:
  kconst init [-d consts dir] [OPTIONS]     This command initializes a new 
                                            constant repository 
  kconst init [-d consts dir] [OPTIONS]


Options:
  -h, --help                        Diplay help usage for kconst
  -v, --version                     Display the current version of kconst
init Options:
  -d, --dir                         Directory for kconst
  -b, --base                        Name for folder of kconst in directory
                                    default is "kconst"
  -B, --noBase                      Place necessary kconst files directly 
                                    into the specified directory outside 
                                    your home folder
```


### Usage
* Simple declarations
````typescript
import { KConst, SelectorItem } from "kconst";
export const kconst = new KConst(args) 
export const K = kconst.K;
export const targets:any = {};


//start builds commads
kconst.collectCallbacks();          //collect all declared callbacks in files with recognized extensions
kconst.startDeclarations();         //Execute the collected callbacks according to the type and the standard and defined priorities
kconst.publish();                   //Publish the exported constants in their respective targets
````

* Values for arguments

````typescript
const args:{
    user:string,                    // current user name with run build default is sysyem user
    root:string,                    // root dir of kconst
    mode:BuildMode,                 // mode of build: "dev" | "prod" | "local" | "remote" | "build" | string;
    name?: string,                  // name for configs default is kconst
    exports?:string,                // directory of exports default is "exports"
    targets?:string,                // directory of targets default is "targets"
    triggers?:string,               // directory of triggers default is "triggers"
    events?:string                  // directory of events default is events
    formatter?: FormatterType,      // format for name of constants: 
                                        // FormatterType.REAL_NAME
                                        // FormatterType.UPPER
                                        // FormatterType.LOWER
    priorityOrder?:SelectorItem[],  // priority order: first run in first array possition : default is [ SelectorItem.DEFAULT, SelectorItem.MODE, SelectorItem.USER, SelectorItem.MODE_USER ]
    reverse?:boolean
} = { };
````


* Simple targets [targets/*/<<filename>>.target.ts]() or [*/<<filename>>.kconst.ts]() ('*' represent any location)


````typescript
kconst.target(target => {
    targets.simpleNodeFile = target.nodeJs( { className: "K", typescript: true, dir: "./autogen/node", })
    targets.simpleJavaFile = target.java( { className: "K", packageName: "com.simple.kconst", dir: "./autogen/node" })
});
````


* Simple constant declaration in [exports/*/<<filename>>.export.ts]() or [*/<<filename>>.kconst.ts]() ('*' represent any location)

````typescript
kconst.declares( (exports, override, SELF_NAME, props) => {
    K.SIMPLE_CONST = SELF_NAME; exports( targets.simpleNodeFile  ); //Use selfe name of constant as value
    K.SIMPLE_HELO_WORD = "HELLO WORD"; exports( targets.simpleNodeFile ); //Use a default value to constant
})
````


* Override a constant declaration in [exports/*/<<filename>>.export.ts]() or [*/<<filename>>.kconst.ts]() ('*' represent any location)

````typescript
//Override all declaratin in this callback block
kconst.declares( (exports, override, SELF_NAME, props) => {
    props.overrideBlock(); 
    K.SIMPLE_HELLO_WORD = "Hello word override in block declaration"; exports( targets.simpleNodeFile );
}, { mode: "dev" })


//Override specific declaration
kconst.declares( (exports, override, SELF_NAME, props) => {

    props.override( () => K.SIMPLE_HELLO_WORD = "Hello word override specific declaration" );
}, { mode: "dev", user:"zootakuxy" });
````

* Define trigger for execute specific action in [triggers/*/<<filename>>.trigger.ts]() or [*/<<filename>>.kconst.ts]() ('*' represent any location)
````typescript
kconst.trigger( TriggersMethod.DECLARE, TriggerEvent.BEFORE, () => {
    //TODO declare body trigger here
});
````

* Define event for create or change constantes in [events/*/<<filename>>.event.ts]() or [*/<<filename>>.kconst.ts]() ('*' represent any location)
````typescript
import {Declaration} from "kconst";

//Event for listem all constant declarations
kconst.on( ( key,  value, oldValue, replace) => {
    //TODO listener const change here
})

//Event for listem specifics declarations in override
kconst.on( ( key,  value, oldValue, replace) => {
    //TODO listener const change here
}, { applyOn: ["SIMPLE_HELLO_WORD", "SIMPLE_HELLO_WORD_2" ], declarations: Declaration.CREATE })
````

* default selector to apply in (KConst.target(), KConst.declares(), KConst.trigger(), KConst.on() }
````typescript
export type Selector = {
    user?:string|string[],          //Specific user to select
    mode?:BuildMode|BuildMode[],    //Specific mode to select
    label?:string                   //Auxiliar label to help in debug
}
````

### A simple args config with Arguments dependency using command line args

index.ts
````typescript
import {Arguments} from "zoo.util/lib/arguments";

const args = new Arguments<{ user:string, mode:string }>( true );
args.argument = { name: "user", alias: "u", value: os.userInfo().username, type: String };
args.argument = { name: "mode", alias: "m", value: "dev" };

export const kconst = new KConst({
    root: __dirname,
    user: args.values.user,
    mode: args.values.mode,
    name: "K",
    targets: "targets",
    triggers: "triggers",
    exports: "exports",
    events: "events",
    priorityOrder: [ SelectorItem.USER ]
});
````

Run using arguments
````shell
# using:
node index --user <<userName>> --mode <<buildMode>>
# or using:
node index -u <<userName>> -m <<buildMode>>
````

## Help

Any advise for common problems or issues.
```
kconst --help
```

## Authors

Contributors names and contact info

[@zootakuxy](https://t.me/zootakuxy)
[@kadafi]()
